const express = require('express');
const app = express();

/////////////////////////////////////////////////////////////////////////////////////
//app.use(require('./registros_constancias/route_constancias'));
app.use(require('./registros_reconocimientos/route_reconocimientos'));
app.use(require('./registros_usuarios/route_usuarios'));
app.use(require('./registros_constancias/route_constancias'));

/////////////////////////////////////////////////////////////////////////////////////
module.exports = app;