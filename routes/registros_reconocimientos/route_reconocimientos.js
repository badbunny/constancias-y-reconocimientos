const express = require('express');
const { Router } = require('express');
const model_users = require('../../models/reconocimiento/newReconocimiento');
//const md5 = require('bcryptjs');
const app = Router();

/// registrar un reconocimiento
app.post('/new/reconocimiento', (req, res) => {
    let body = req.body;
    let registroReconocimiento = new model_users({
        Nombre:body.Nombre,
        Rol:body.Rol,
        NombreP: body.NombreP,
        Clave: body.Clave,
        FechaI: body.FechaI,
        FechaC: body.FechaC,
        FechaE: body.FechaE,
        Horas: body.Horas
    });

    registroReconocimiento.save();

    res.status(200).json({
        ok: true,
        registroReconocimiento,
        msg: "Registro exitoso"
    });
});

// buscar reconocimiento
app.get('/obtener/datos/reconocimientos', async (req, res) => {
    const respuesta = await model_users.find();
    res.status(200).json({
        ok: true,
        respuesta
    });
});


// eliminar reconocimiento
app.delete('/delete/registro/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await model_users.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        respuesta,
        msg: "REGISTRO ELIMINADO EXITOSAMENTE"
    });
});

//actualizar usuario
app.put('/update/registro/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    //delete campos.nombre
    //delete campos.apellidos
    //delete campos.role

    const respuesta = await model_users.findByIdAndUpdate(id, campos, { new: true });

    res.status(202).json({
        ok: true,
        respuesta,
        msg: "ACTUALIZADO EXITOSAMENTE"
    });
});

module.exports = app;