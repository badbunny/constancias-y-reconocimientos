const {Schema, model} = require('mongoose');

const reconocimientosSchema = Schema ({
    Nombre:{type:String},
    Rol:{type:String},
    NombreP: {type: String},
    Clave: {type: String},
    FechaI: {type:Date},
    FechaC: {type: Date},
    FechaE: {type: String},
    Horas: {type: Number},
    idUsuario: {type: Schema.Types.ObjectId, ref: 'usuarios'}
});

module.exports = model('reconocimientos',reconocimientosSchema);